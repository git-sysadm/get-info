# get-info

Version    : 0.1
License    : MIT License

 - get-info is a script to get details about available free disk space
   and network status of remote machine.

 - This script require SSh.
   - At first configure communication keys by adding switch --add and
     address of target machine.

     `get-info --add <username>@<IP Address>`



 * To get-info of remote machine use below example command.

   `get-info check <username>@<IP Address>`


 * Simple command. To make it simple you can ignore command if the logged
   in user name exist on remote machine with enabled shell then you can
   use below simple command.

   `get-info check <IP Address>`

    Or

   `get-info check <Hostname>`

   If hostname is able to resolve in IP.


For more about license use command

  `get-info license`


<br><br><br>


#### Tip: For quick result disable UseDNS `UseDNS no` option on remote SSh configuration (/etc/ssh/sshd_config).
